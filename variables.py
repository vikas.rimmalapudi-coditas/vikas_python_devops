# a variable is used to assign a name to some memory location
name="vikas"        #string data type
age=21              #integer  data type

#21 is stored in  memory location and the name given to that memory location is age..

print(type(age)) # type() is  used to identify the data type of the variable

weight=69.5  #float data type
print(str(weight)+" "+"kgs") #type casting -converting float to string

boolean=True  #boolean data type


#assignment:-  assign multiple values in one line and same value to multiple variables
name,age,weight='vikas',21,69.5
vikas,venkat=True


