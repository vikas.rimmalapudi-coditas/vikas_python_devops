#easy way to create list with less syntax
squares=[i**2 for i in range(1,11)]
print(squares)


marks=[41,55,21,33,66,77,11]
print([i if i>=40 else 'failed' for i in marks])