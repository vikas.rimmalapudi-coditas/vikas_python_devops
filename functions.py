#functions are used for code reusability we can use any n number of times
#there are 4 types of funcs in python 1)keyworded funs 2)default args 3)positional args 4)lambda funs

def is_prime(n):
    if n<2:
        print('not prime')
        return
    for i in range(2,int(n**0.5)+1):
        if n%i==0:
            print("not prime")
            return
    else:
        print('prime')        
is_prime(9)
print(is_prime.__doc__)

#positional args
def positional_fun(age,name,city):
    print(f"my name is {name} and i'm {age} years old and i'm from {city}")
positional_fun(21,'vikas','ap')

#keyworded args
def positional_fun(name,age,city):
    print(f"my name is {name} and i'm {age} years old and i'm from {city}")
positional_fun(age=21,name='vikas',city='ap')