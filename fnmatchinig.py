import fnmatch

files = [ "lists.py", "freecodecamp_certificate.png",  "main.py"]


def py_matches(files):

    matches = fnmatch.filter(files, "*.py")
    return matches


print(f"Found matches: {py_matches(files)}")


from shutil import make_archive
import os

username = "user1"
root_dir = "C:\\Users\\Coditas\\Desktop\\py_devops"
path = f"{root_dir}/{username}"

make_archive(username, "gztar", root_dir)

make_archive(username, "zip", root_dir)

print(os.listdir("/tmp"))