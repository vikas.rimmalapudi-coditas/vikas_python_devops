name='vikas'
print(len(name)) # len() is used to find the length
print(name.capitalize()) # capitalize() is used to capitalize the first letter
print(name.upper()) #used to capitlize  everything
print(name.lower()) #lower() is used to lower everything
print(name.isalnum()) #isalnum return boolean True if string contains alphabets and numbers
print(name.isalpha()) #is used to return boolean True if string contains alphabets
print(name.lstrip()) #used to return new string with removing starting whitespaces
print(name.rstrip()) # returns a new string with trailing whitespace removed
