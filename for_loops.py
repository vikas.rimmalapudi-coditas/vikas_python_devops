#for loops are used when we know how many  iterations we have to do ahead of time
#One main difference is while loops are best suited when you do not know ahead of time the number of iterations that you need to do. When you know this before entering the loop you can use for loop.
arr=[]
#range(start,end(exclusive),step)
for i in range(0,100,1):
    arr.append(i)
print(arr)   