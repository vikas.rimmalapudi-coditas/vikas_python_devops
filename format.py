#used to return in specific format
name='vikas'
age=20
print(f'hello {name},your age is {age}')
print("hello {} ,you are {} years old".format(name,age))
print("hello {1} ,you are {0} years old".format(name,age)) #positional args

pi=3.14121
print('{:.2f}'.format(pi))  #f is floating point 
print('{: b}'.format(age)) # for binary  format 
#for hex and oct use x and o
