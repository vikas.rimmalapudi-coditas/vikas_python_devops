#sets are collection of unique elemenst in unordered way..
#set can contains one null value and no duplicates and they are represented in {}
nums=set(range(1,10))
print(nums)
nums.add(11)
print(11 in nums)
nums.add(1) #raises error ..

nums.remove(2)
print(nums)
arr=[11,111,7,6]
nums.update(arr)   #The update() method updates the current set, by adding items from another set (or any other iterable).
print(nums)
